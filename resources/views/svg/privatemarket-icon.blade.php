<svg width="28px" height="32px" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 28.3 32" style="enable-background:new 0 0 28.3 32;" xml:space="preserve" class="privatemarket-icon">
  <g>
  	<path class="privatemarket-icon--item" d="M14.2,0l14,8v16l-14,8l-14-8V8L14.2,0z"/>
  </g>
</svg>
