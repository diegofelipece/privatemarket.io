@extends('layouts.app')

@section('content')

  @include('partials.home-hero')
  @include('partials.home-numbers')
  @include('partials.home-featured')
  @include('partials.home-advisors')
  @include('partials.home-managers')
  @include('partials.home-features')
  @include('partials.home-partners')
  @include('partials.home-contact')

@endsection
