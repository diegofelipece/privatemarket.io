@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    @if( have_rows('page_sections') )

        @while(have_rows('page_sections')) @php the_row() @endphp

            @if( get_row_layout() == 'section_hero' )
              @include('partials.section-hero')
            @elseif( get_row_layout() == 'section_numbers' )
              @include('partials.section-numbers')
            @elseif( get_row_layout() == 'section_brands' )
              @include('partials.section-brands')
            @elseif( get_row_layout() == 'section_list_left' )
              @include('partials.section-list-left')
            @elseif( get_row_layout() == 'section_list_right' )
              @include('partials.section-list-right')
            @elseif( get_row_layout() == 'section_text_list' )
              @include('partials.section-text-list')
            @endif

        @endwhile

    @else
    @endif

  @endwhile
@endsection
