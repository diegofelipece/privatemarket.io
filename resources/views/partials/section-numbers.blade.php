<section class="section-padding" data-numbers="{{ the_sub_field('section_numbers_url') }}">
  <div class="grid-x">

    <div class="cell">
      <div class="grid-x">
        <div class="cell medium-2 large-1 show-for-medium"><div class="square-box gray"></div></div>

        <div class="cell medium-offset-1 medium-4 large-offset-3 large-3">
          <div class="grid-x grid-padding-x medium-padding-collapse">
            <div class="section-subtitle cell">
              <p class="section-subtitle--elm">{{ the_sub_field('section_numbers_description') }}</p>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="cell medium-offset-2 medium-2 large-offset-1 large-1 small-4">
      <div class="square-box dark" data-stat="4">
        @include('partials.loader')
      </div>
    </div>

    <div class="cell medium-4 large-6 show-for-large"></div>

    <div class="cell medium-4">
      <div class="grid-x">

        <div class="cell medium-4 large-offset-2 large-2 small-4"><div class="square-box gray"></div></div>

        <div class="cell medium-4 large-2 small-4">
          <div class="square-box primary" data-stat="3">
            @include('partials.loader')
          </div>
        </div>

        <div class="cell medium-2 small-4 show-for-large"></div>

        <div class="cell medium-4 large-2 small-4">
          <div class="square-box dark" data-stat="5">
            @include('partials.loader')
          </div>
        </div>

        <div class="medium-offset-4 cell medium-2 small-4 show-for-large"><div class="square-box gray"></div></div>

      </div>
    </div>

    <div class="cell medium-4 large-2">
      <div class="square-box primary large" data-stat="1">
        @include('partials.loader')
      </div>
    </div>

    <div class="cell medium-4 large-2">
      <div class="grid-x">

        <div class="medium-offset-4 cell medium-4 small-4">
          <div class="square-box dark" data-stat="2">
            @include('partials.loader')
          </div>
        </div>

        <div class="cell medium-4 small-4"><div class="square-box gray"></div></div>

      </div>
    </div>

    <div class="cell medium-2 large-offset-2 large-1 small-4"><div class="square-box gray"></div></div>

  </div>
</section>
