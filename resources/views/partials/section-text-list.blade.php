<section class="section-padding">

  <div class="grid-x grid-padding-x medium-padding-collapse">

    <div class="section-title cell medium-offset-1 medium-6 large-4">
      <h3 class="section-title--elm">{{ the_sub_field('section_text_list_title') }}</h3>
    </div>

    <div class="section-subtitle cell medium-offset-2 medium-5 large-4">
      <p class="section-subtitle--elm">{{ the_sub_field('section_text_list_text') }}</p>
    </div>

    @if( have_rows('section_text_list_itens') )
      <div class="cell medium-offset-2 medium-5 large-4">
        <div class="grid-x">

          @while ( have_rows('section_text_list_itens') ) @php the_row() @endphp

            <div class="cell medium-2">
              <img src="{{ the_sub_field('section_text_list_itens_image') }}" alt="">
            </div>
            <div class="cell medium-6">
              <h3>{{ the_sub_field('section_text_list_itens_title') }}</h3>
              <p>{{ the_sub_field('section_text_list_itens_text') }}</p>
            </div>

          @endwhile

        </div>
      </div>
    @endif

  </div>

</section>
