<footer class="footer">
  <div class="grid-x grid-padding-x medium-padding-collapse">
    <div class="cell medium-offset-1 medium-1 large-4">
      <a class="brand" href="{{ home_url('/') }}">
        @include('svg.privatemarket-icon')
      </a>
    </div>

    <div class="cell medium-offset-2 medium-4 large-offset-0 large-2">
      <div class="grid-x">

        @if (has_nav_menu('footer-col-1'))
          <div class="cell medium-4">
            {!! wp_nav_menu(['theme_location' => 'footer-col-1', 'menu_class' => 'menu vertical']) !!}
          </div>
        @endif
        @if (has_nav_menu('footer-col-2'))
          <div class="cell medium-4">
            {!! wp_nav_menu(['theme_location' => 'footer-col-2', 'menu_class' => 'menu vertical']) !!}
          </div>
        @endif

        <div class="cell footer--copyright">
          © {{ date("Y") }} Privatemarket Technologies, Ltd.<br>
          Designed by <a href="https://www.mozestudio.com/" target="_blank">Moze</a>
        </div>

      </div>
    </div>

  </div>
</footer>
