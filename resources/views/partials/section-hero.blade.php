<section class="hero section-padding">
  <div class="grid-x grid-padding-x medium-padding-collapse">

    <div class="medium-offset-1 cell medium-5 hero--title">
      <h1 class="hero--title-elm">{{ the_sub_field('section_hero_text') }}</h1>
    </div>
    <div class="medium-offset-2 cell medium-2">
      <a href="{{ the_sub_field('section_hero_button_url') }}" class="button expanded">{{ the_sub_field('section_hero_button_label') }}</a>
    </div>

  </div>
</section>
