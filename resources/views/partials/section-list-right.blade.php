<section class="section-padding">

  <div class="grid-x">

    <div class="cell large-4 small-order-2 medium-order-1 hide-for-medium-only">
      <div class="grid-x">

        <div class="medium-offset-2 cell medium-2 small-2"><div class="square-box gray"></div></div>

        <div class="medium-offset-2 cell medium-4 small-4">
          <div class="square-box primary" style="background-image: url(@asset('images/graph-2.svg'))"></div>
        </div>

        <div class="cell medium-2 show-for-medium"></div>
        <div class="cell medium-2 small-2 small-offset-6 medium-offset-0"><div class="square-box gray"></div></div>

      </div>
    </div>

    <div class="cell medium-6 medium-offset-1 large-offset-0 large-3 small-order-1 medium-order-2">
      <div class="grid-x grid-padding-x medium-padding-collapse">
        <div class="section-title cell">
          <h3 class="section-title--elm">{{ the_sub_field('section_list_right_title') }}</h3>
        </div>


        @if( have_rows('section_list_right_itens') )
          <div class="cell">
            <ol class="special-list">

              @while ( have_rows('section_list_right_itens') ) @php the_row() @endphp

                <li>
                  <p>{{ the_sub_field('section_list_right_itens_item') }}</p>
                </li>

              @endwhile

            </ol>
          </div>
        @endif

      </div>
    </div>

  </div>

</section>
