<header class="header">
    <div class="grid-x grid-padding-x medium-padding-collapse">

      <div class="medium-offset-1 cell medium-2 small-6">
        <a class="brand" href="{{ home_url('/') }}">
          @include('svg.privatemarket')
        </a>
      </div>

      <div class="cell medium-4 small-2 text-right hide-for-large" data-responsive-toggle="responsive-menu">
        <button class="menu-button" type="button" data-toggle="responsive-menu"></button>
      </div>

      <div class="cell large-4 hide-for-large" id="responsive-menu" data-hide-for="large">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'menu mobile-menu align-center vertical']) !!}
        @endif
      </div>

      <div class="cell large-4 show-for-large">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'menu align-right']) !!}
        @endif
      </div>

    </div>
</header>
