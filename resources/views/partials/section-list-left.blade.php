<section class="section-padding">

  <div class="grid-x">

    <div class="cell medium-offset-1 medium-6 large-3 section-padding">
      <div class="grid-x grid-padding-x medium-padding-collapse">

        <div class="section-title cell medium-6">
          <h3 class="section-title--elm">{{ the_sub_field('section_list_left_title') }}</h3>
        </div>


        @if( have_rows('section_list_left_itens') )
          <div class="cell">
            <ol class="special-list">

              @while ( have_rows('section_list_left_itens') ) @php the_row() @endphp

                <li>
                  <p>{{ the_sub_field('section_list_left_itens_item') }}</p>
                </li>

              @endwhile

            </ol>
          </div>
        @endif

      </div>
    </div>

    <div class="cell hide-for-medium-only medium-4">
      <div class="grid-x">

        <div class="medium-offset-4 cell medium-2 small-2"><div class="square-box gray"></div></div>

        <div class="medium-offset-4 cell medium-4 small-4">
          <div class="square-box primary" style="background-image: url(@asset('images/graph-1.svg'))"></div>
        </div>

        <div class="medium-offset-2 cell medium-2 small-offset-6 small-2"><div class="square-box gray"></div></div>

      </div>
    </div>

  </div>

</section>
