<section class="featured section-padding">
  <div class="grid-x grid-padding-x medium-padding-collapse">

    <div class="section-subtitle medium-offset-1 cell medium-6">
      <h3 class="section-subtitle--elm">{{ the_sub_field('section_brands_title') }}</h3>
    </div>

    @if( have_rows('section_brands_images') )
      <div class="medium-offset-1 cell medium-6">
        <div class="grid-x grid-margin-y grid-margin-x align-justify text-center align-middle">

          @while ( have_rows('section_brands_images') ) @php the_row() @endphp

            <div class="cell medium-auto large-shrink">
              @php $brand_url = get_sub_field('section_brands_images_url') @endphp
              @if($brand_url)<a href="{{ $brand_url }}">@endif
                <img src="{{ the_sub_field('section_brands_images_img') }}" alt="{{ the_sub_field('section_brands_images_label') }}">
              @if($brand_url)</a>@endif
            </div>

          @endwhile

        </div>
      </div>
    @endif

  </div>
</section>
