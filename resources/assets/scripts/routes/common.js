export default {
  init() {
    // JavaScript to be fired on all pages
    let numbersSection = $('*[data-numbers]');
    if (numbersSection.length) {

      if ($('*[data-stat]').length) {

        $.ajax({
          url: numbersSection.data('numbers'),
          type: 'GET',
          dataType: 'json',
        })
        .done(function(data) {
          data.stats.map(stat => {
            let target = $(`*[data-stat="${stat.id}"]`);
            if (target.length) {
              target.html(`
                <div class="square-box--content">
                  <p class="data-number">${stat.value}</p>
                  <p class="data-label">${stat.label}</p>
                </div>
              `);
            }
          });
        })
        .fail(function(data) {
          throw new Error(data);
        });
      }
    }

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
